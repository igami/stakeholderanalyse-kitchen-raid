<h1>Stakeholder Analyse als Grundlage zur Definition von Projektzielen für das Projekt Kitchen Raid des Makerspace Gütersloh e.V.</h1>

- [1. Einleitung](#1-einleitung)
  - [1.1. Die Maker-Bewegung und deren Orte der Zusammenkunft](#11-die-maker-bewegung-und-deren-orte-der-zusammenkunft)
  - [1.2. Der Hintergrund zu dem Projekt Kitchen Raid](#12-der-hintergrund-zu-dem-projekt-kitchen-raid)
- [2. Erläuterung von Stakeholdermanagement und Projektzielen in der Theorie](#2-erläuterung-von-stakeholdermanagement-und-projektzielen-in-der-theorie)
- [3. Durchführung der Stakeholder Analyse in der Praxis](#3-durchführung-der-stakeholder-analyse-in-der-praxis)
  - [3.1. Die Identifizierung der Stakeholder](#31-die-identifizierung-der-stakeholder)
  - [3.2. Die Erschaffung der Personas](#32-die-erschaffung-der-personas)
  - [3.3. Entwickeln eines Plans zur Einbindung der Stakeholder](#33-entwickeln-eines-plans-zur-einbindung-der-stakeholder)
- [4. Fazit](#4-fazit)
- [5. Literatur- und Quellenverzeichnis](#5-literatur--und-quellenverzeichnis)

# 1. Einleitung

Ziel der Arbeit ist die Stakeholder Analyse zu dem Projekt Kitchen Raid des Makerspace Gütersloh. Mit der Stakeholder Analyse im Allgemeinen und der Betrachtung der Nutzergruppen, in Form von Personas, im Speziellen sollen die Projektziele konkretisiert werden, sodass die nächsten Planungsschritte erfolgen können, um Fördermittel zu beantragen und einen neuen Vereinsraum anzumieten und einzurichten.

Kapitel Eins gibt eine Einführung in die Maker-Bewegung und deren Orte der Zusammenkunft – Makerspaces, FabLabs und Hackerspaces. Weiterhin wird der Hintergrund zu dem Projekt Kitchen Raid beschrieben.

Kapitel Zwei befasst sich mit den Angaben aus der Literatur zu Stakeholdern im Allgemeinen und Personas im Speziellen, sowie der Bedeutung von Zielen.

Kapitel Drei erläutert die Durchführung der Stakeholder Analyse in der Praxis. Dabei werden lediglich die Prozesse betrachtet, die in der Projektinitialisierungsphase behandelt werden. Es wird beschrieben wie die Stakeholder identifiziert und die Personas erschaffen wurden.

Das letzte Kapitel fasst das Vorgehen insgesamt zusammen und gibt einen Ausblick wie das Projekt weitergeführt werden kann. Es wird betrachtet wie sich Theorie und Praxis voneinander unterschieden haben und ob das Vorgehen für zukünftige Projekte geeignet ist.

## 1.1. Die Maker-Bewegung und deren Orte der Zusammenkunft

Die Maker-Bewegung besteht aus unterschiedlichsten Individuen, die durch verschiedene Eigenschaften verbunden werden. Es sind Personen die es lieben Dinge für sich selbst herzustellen oder zu erweitern. Dabei stehen der Spaß bei der Entwicklung und der Stolz auf das fertige Produkt im Vordergrund. Die Arbeit umfasst dabei eine Vielzahl verschiedener Bereiche, vom traditionellen Handwerk bis zur Hightech-Elektronik. Es ist eine Kultur des Teilens und der gegenseitigen Unterstützung, in der es kein primäres Gewinnstreben gibt. Es gilt offene Standards und freie Lizenzen zu verwenden, damit andere die eigene Arbeit wiederum für sich anpassen können. Maker sind wissbegierig und zugleich Wissensvermittler. Sie lernen voneinander und treiben sich gegenseitig zu Höchstleistungen an, wodurch nahezu marktreife Produkte mit sehr kleinem Budget erschaffen werden. (vgl. Bidermann 2016, S. 44-48; vgl. Maker Media 28.06.2021)  
Hatch fasst diese Eigenschaften als neun Grundelemente eines Makers zusammen: Make, Share, Give, Learn, Tool Up, Play, Participate, Support und Change. (Hatch 2013, S. 1f)

Um sich besser untereinander auszutauschen und das Arbeiten mit teuren oder speziellen Maschinen zur ermöglichen organisieren sich die Maker oft in regionalen Gruppen als Makerspace, Hackerspace oder FabLab. Eine Unterscheidung zwischen diesen Formen ist nicht trennscharf möglich. Sie haben sich jeweils unabhängig voneinander entwickelt und überschneiden sich hinsichtlich Struktur, Ausstattung und der Art und Weise der Nutzung. Gemeinhin lassen sie sich als offene Werkstätten definieren an denen Maschinen und Werkzeuge gemeinschaftlich genutzt und erforscht werden. Somit erlangen Privatpersonen die Möglichkeit industrielle Produktionsverfahren kennen zu lernen und selbstständig zu nutzen. (vgl. Bidermann 2016, S. 51; vgl. Fastermann 2014, S.57)  
Hackerspaces konzentrieren sich überwiegend auf digitale und elektronische Arbeiten, sowie Technologie im Allgemeinen. (vgl. hackerspaces.org 28.06.2021) FabLabs bzw. Fabrication Laboratorys müssen sich an die Fab Charter halten, welche unter anderem definiert welche Kernausstattung vorhanden ist, wer ein FabLab benutzen darf und welche Verantwortlichkeiten es gibt. (vgl. MIT 2012) Makerspaces sind, wie FabLabs auch, Hightech-Werkstätten deren Ausstattungen sich nach den Makern vor Ort richten.

Wie viele Orte der Zusammenkunft es gibt lässt sich nicht genau erfassen. Maker Media (28.06.2021a) kennt über 300 Orte im deutschsprachigen Raum in denen sich Maker treffen. Daneben listet hackerspaces.org (28.06.2021a) 139 Orte alleine in Deutschland auf. Zwischen diesen beiden Quellen gibt es Überschneidungen, ebenso wie bei der Anzahl von weltweit über 1.750 FabLabs in mehr als 100 Ländern die sich bei der Fab Foundation (28.06.2021) registriert haben.

## 1.2. Der Hintergrund zu dem Projekt Kitchen Raid

Der Makerspace Gütersloh wurde 2018 in der Stadtbibliothek Gütersloh gegründet und fand dort bis Ende 2019 ein Zuhause. Seit Dezember 2019 ist der Verein Teil des Bürgerkiez "Die Weberei". (vgl. Makerspace 29.06.2021) Bereits bei Bezug des Raumes haben die Mitglieder geschwärmt, dass weitere Räume angemietet werden müssen, sobald die regelmäßigen Einnahmen dies erlauben. Aus praktischen Gründen wurde dafür als erstes der Nebenraum ins Auge gefasst, da dieser eine direkte Verbindungstür hat und von der Weberei nur als Abstellraum genutzt wird. Mit der Einrichtung der Textilwerkstatt im Juni 2020 wurde der Bedarf an einem abgedunkelten Raum geschaffen, um die Siebdruckrahmen zu belichten. Damit wurde das erste Mal konkretisiert, dass der Raum angemietet werden soll. Da dieser eine ehemalige Teeküche ist und über eine Spüle verfügt ist der Raum auch gut als (Experimental-)Küche geeignet. Zudem läuft das Förderprogramm "Heimatscheck" NRW noch bis einschließlich 2022 und das Thema "Lebensmittel" bietet eine angemessene Kategorie. (vgl. MHKBG 2021, S. 5, S. 8) In losen Gesprächen wurde immer wieder über eine Küche und einen Raum als Rückzugsort gesprochen. Auf der jährlichen Mitgliederversammlung im Februar 2021 wurde darüber abgestimmt das Projekt Kitchen Raid zu beginnen. (vgl. Makerspace 27.02.2021) Der Projektname ist ein Wortspiel mit Bezug auf den Hersteller "KitchenAid" und soll zum Ausdruck bringen, dass ein neuer Raum erschlossen wird.

# 2. Erläuterung von Stakeholdermanagement und Projektzielen in der Theorie

Als Theorie zu dem Stakeholder Management wird der "PMBOK Guide: PM-Standard des Project Management Institute" herangezogen.

Nach diesem Standard beschreibt das Stakeholder Management alle Prozesse, um Stakeholder zu identifizieren, das Interesse und den Einfluss auf das Projekt zu analysieren und eine Strategie zu entwickeln, um sie effektiv in die Projektentscheidung und -ausführung einzubeziehen. (vgl. PMI 2017, S. 503)

Stakeholder (dt. Projektbeteiligte) sind die "Gesamtheit aller Projektteilnehmer, -betroffenen und -interessierten, deren Interessen durch den Verlauf oder das Ergebnis des Projekts direkt oder indirekt berührt sind. Dazu gehören z.B. Auftraggeber, Auftragnehmer, Projektleiter, Projektmitarbeiter, Nutzer des Projektergebnisses, Arbeitnehmervertretung, Anwohner, Naturschutzverbände, Stadtverwaltung, Banken, Politik usw." (DIN 2009, S.12)

Zu dem Stakeholder Management gehören vier Prozesse mit eigenen Eingangsinformationen, Methoden und Werkzeugen zur Bearbeitung und Ausgangsinformationen (siehe Abb. 1).

1. Stakeholder identifizieren und analysieren
2. Einbindung der Stakeholder planen
3. Einbindung der Stakeholder steuern
4. Einbindung der Stakeholder überwachen

Abb. 1: Project Stakeholder Management Overview  
![](images/Project%20Stakeholder%20Management%20Overview.png)  
Quelle: PMI 2017, S. 504

Bei dem gesamten Stakeholder Management handelt es sich nicht um ein einmaliges Ereignis. Die einzelnen Prozesse werden je nach Bedarf regelmäßig während des Projektes wiederholt. Gründe dafür können der Beginn einer neuen Projektphase oder Änderungen aus anderen Projektprozessen wie dem Problemmanagement sein. (vgl. PMI 2017, S. 505)

Im ersten Prozess werden die Stakeholder identifiziert und analysiert.  
Bei Projektbeginn stehen meist schon Key-Stakeholder, wie Nutzergruppen und Teile des Projektteams oder Vertragspartner, fest. Weitere Stakeholder werden durch Experteninterviews, Umfragen, Brainstormings und andere Kreativitätstechniken identifiziert.  
Direkt im Anschluss an die Identifizierung erfolgt die Analyse der Stakeholder hinsichtlich ihrer Interessen, ihres Einflusses und damit ihrer möglichen Auswirkungen auf den Projekterfolg. Übliche Methoden sind die Bewertung in einer Interessen-Einfluss-Matrix und der Abfrage der positiven oder negativen Einstellung zu dem Projekt. (vgl. PMI 2017, S. 507-513)  
Für die Berücksichtigung der Nutzergruppen im Speziellen kann die Persona-Methode genutzt werden. Dabei werden fiktive Personen erschaffen, die für eine Bedarfs- und Zielgruppenanalyse herangezogen werden. Diese Personas müssen einen Namen haben und stellen ein konkretes Individuum dar. Die Anzahl der Personas muss dabei so gewählt werden, dass sie die Vielfalt heterogener Zielgruppen darstellen. (vgl. Cooper 2004, S. 123-135; vgl. Lepzien/Lewrenz 2017, S. 30)  
Alle so gewonnenen Informationen werden in einem Stakeholder Register erfasst. (vgl. PMI 2017, S. 514)

Im zweiten Prozess wird ein Plan erstellt, um die Stakeholder, basierend auf ihren Bedürfnissen und Erwartungen, möglichst effektiv in den Projektablauf einzubinden.  
Übliche Bestandteile des Plans sind die Priorisierung der Stakeholder, basierend auf Interesse und Einfluss und die Darstellung der gewollten (Desired) und aktuellen (Current) Beteiligung in einer Matrix (siehe Abb. 2). (vgl. PMI 2017, S. 516-522)

Abb. 2: Stakeholder Engagement Assessment Matrix  
![](images/Stakeholder%20Engagement%20Assessment%20Matrix.png)  
Quelle: PMI 2017, S. 522

Der Plan sollte so aufgebaut sein, dass Strategien und Maßnahmen ersichtlich sind, um die produktive Einbindung der Stakeholder in die Entscheidungsfindung und Ausführung zu fördern. Dabei gibt es keine Vorgaben zur Detaillierung oder Gestaltung. Der Plan wird individuell ausgearbeitet und richtet sich nach den Bedürfnissen des Projekts und den Erwartungen der Stakeholder. (vgl. PMI 2017, S. 522)

Anhand der Grundlagen aus den ersten beiden Prozessen erfolgt im dritten Prozess die Steuerung der Stakeholder.  
Es ist der Prozess der Kommunikation und Arbeit mit den Stakeholdern, um ihre Bedürfnisse und Erwartungen zu erfüllen, Probleme rechtzeitig anzusprechen und eine angemessene Beteiligung zu fördern. Der Prozess soll dem Projektmanager ermöglichen die Unterstützung der Stakeholder zu erhöhen und ihren Widerstand zu minimieren, indem sie die Projektziele, den Nutzen und die Risiken des Projekts klar verstehen und begreifen welchen Einfluss ihre Beteiligung auf den Projekterfolg hat. Übliche Methoden für die Steuerung sind Einzelgespräche oder Besprechungen. Dabei sollten die Projektbeteiligten angemessene Kommunikationsfähigkeiten besitzen, um Missverständnisse zu vermeiden. (vgl. PMI 2017, S. 523-528)

Im vierten und letzten Prozess wird die Einbindung der Stakeholder überwacht und bewertet.  
Dieser Prozess soll Abweichungen zwischen den gewollten Auswirkungen der Arbeit mit den Stakeholdern und den eingetroffenen Auswirkungen sichtbar machen, die Ursachen dafür analysieren und den Plan diesbezüglich anpassen. Hierfür kann es notwendig sein wieder den ersten Prozess zu durchlaufen und die Stakeholder erneut zu analysieren. (vgl. PMI 2017, S. 530-536)

Das Stakeholder Management kann entscheidend für den Projekterfolg sein. Werden Stakeholder nicht berücksichtigt oder in angemessener Weise eingebunden können sie den Unterschied zwischen Projekterfolg und -misserfolg bedeuten. Werden die Stakeholder nicht richtig analysiert können sich falsche Projektziele ergeben. (vgl. PMI 2017, S. 503)  
Bei 68% der gescheiterten Projekte ist insbesondere eine fehlende, konkrete Zieldefinition der Grund für das Scheitern gewesen. (PA/GPM 2004 S. 2) Daher sollten früh im Projekt Ziele formuliert werden, die den Sollzustand beschreiben. (vgl. Brüning/Schwanitz 2018, S. 29) Insbesondere im Ehrenamt ist eine gute und richtige Zieldefinition besonders wichtig, damit sich die Freiwilligen für das Projekte interessieren und einsetzen. Ist diese nicht vorhanden sollte das Projekt nicht gestartet werden. (vgl. Braune/Alberternst 2013, S. 206)

# 3. Durchführung der Stakeholder Analyse in der Praxis

Das Projekt Kitchen Raid befindet sich noch in der Initialisierungsphase, sodass nur die ersten beiden Prozesse des Stakeholder Managements durchgeführt werden, um die Projektziele zu konkretisieren. Es gibt kein festes Projektteam. Die Organisation des Makerspace Gütersloh findet weitestgehend eigenständig durch die Mitglieder statt. Dafür gibt es ein Forum und eine Chatgruppe zur asynchronen Kommunikation und die Community-Runde, ein wöchentliches Meeting, zur synchronen Kommunikation.

## 3.1. Die Identifizierung der Stakeholder

Wie im Prozess "Identifizierung und Analyse der Stakeholder" beschrieben konnten zuerst einige Key-Stakeholder identifiziert werden:

1. Der Verein "Makerspace Gütersloh" als Mieter des Raumes und Eigentümer der Einrichtung
2. "Die Weberei" Gütersloh als Vermieter des Raumes
3. Das Förderprogramm "Heimatscheck" vom Land NRW als primäre Finanzierungsquelle (bereits bekannt durch die Finanzierung der Textilwerkstatt)
4. Michael Prange als Projektkoordinator (hat die bisherigen Förderanträge koordiniert)
5. Die Mitglieder des Vereins als Nutzer der Projektergebnisse und Projektteam

Um weitere Stakeholder zu identifizieren, wurde ein Brainstorming in der Gruppe durchgeführt.  
Zur Erfassung der Nutzerinteressen wurden Anwendungsfälle gesammelt, die vorher schon in Einzelgesprächen geäußert, oder durch das Brainstorming erkannt wurden.
Im Anschluss daran sollte die Gruppe noch folgende Fragen beantworten:

- Was für regionale Unternehmen gibt es die sich aus ideellen oder PR Gründen an dem Projekt beteiligen wollen?
- Wem gehört der Raum bzw. wer muss gefragt werden für Renovierung und bauliche Änderungen?  
  (Es muss eine Dunstabzugshaube installiert werden und sichergestellt werden, dass die Lüftungsanlage funktioniert, da es sich um einen innenliegenden Raum handelt.)
- Was für regionale Vereine gibt es die ein Interesse daran haben können die Möglichkeiten zu nutzen?

Zum Abschluss des Brainstormings wurden noch die möglichen Risiken festgehalten, die durch die Anwendungsfälle und die bisher identifizierten Stakeholder auftreten können.

Die Ergebnisse wurden in einem CryptPad, ein kollaboratives Dokument, gesammelt und im Forum zur Diskussion gestellt. Dadurch konnten Mitglieder, die bei dem Brainstorming nicht teilnehmen konnten, ihre Ideen einbringen. Weiterhin wurden die Ergebnisse bei einer Community-Runde vorgestellt. Als es keine weiteren Vorschläge für die Stakeholder gab wurde die initiale Identifizierung als abgeschlossen erklärt.

Auf diese Weise konnten 31 Anwendungsfälle, 8 Stakeholder und 12 Risiken identifiziert werden.

- Anwendungsfälle (Rohdaten vom Brainstorming):  
  Dunkelkammer für Textilwerkstatt • molekulare Küche (Bubble-Tea/Melonen Kaviar) • Lagerung von sauberen Maschinen • verdeckte Lagerung von Kamera Material • Bierbrauen • Vakuuminfusion • vegane Lebensmittel herstellen • Gefriertrocknen • Foodhacking • Mikroskopie • Kochen • Wecken • Dörren • (Brot) Backen (Backofen) (Hohe Temperaturen?) • Backstein für Ofen? Elektro-Steinbackofen? • Fermentieren • Sitzgelegenheit/ruhiger Arbeitsplatz • Kaffee etc. rösten • Mixen (Drinks etc.)/Blender • Papierarbeit (Papierschöpfen etc.) • Tastings (z.B. Bier) • Dämpfen • Sous-Vide-Garen • Branntwein brennen? (Anmeldepflichtig!) • Aromatisieren durch Einlegen (Essig, Öl, Rum, ...) • Seife und andere Pflegeprodukte herstellen • Filme auf dem Beamer schauen/Chaos Everywhere • Tonarbeiten/Modellieren • Pflanzen (Pilze, Kräuter, ...) in der Küche • Tiefkühlpizza oder schnelles Abendessen zubereiten • Tiefziehbox
- Stakeholder:
  - Was für regionale Unternehmen gibt es die sich aus ideellen oder PR Gründen an dem Projekt beteiligen wollen?
    - Dr. Oetker
    - Schenke
    - Nobilia
    - Mestemacher
    - Brauhaus Gütersloh
    - Miele
  - Wem gehört der Raum bzw. wer muss gefragt werden für Renovierung und bauliche Änderungen?
    - Die Weberei (Vermieter des Raums)
    - Stadt Gütersloh (Eigentümer der Weberei)
  - Was für regionale Vereine gibt es die ein Interesse daran haben können die Möglichkeiten zu nutzen?
    - Suppenküche e.V.
- Risiken (Rohdaten vom Brainstorming):
  - Pflanzen in der Küche werden nicht gepflegt
  - Tiefkühlpizza oder schnelles Abendessen zubereiten sorgt für dreckiges Geschirr
  - Bei Tonarbeiten/Modellieren muss der Backofen anschließend auslüften
  - Tiefziehbox benötigt einen separaten Backofen
  - Wegerecht vereinbaren (Serverraum nur über durch die Küche zugänglich)
  - Lichtquelle
  - Lüftung wurde vermutlich lange nicht genutzt
  - Abluft gibt es nicht, ist für eine Abzugshaube erforderlich
  - Wasser läuft momentan nur langsam aus der Leitung, eventuell nur abgesperrt oder die Rohre sind verkalkt
  - Umgang mit verwinkeltem Raum
  - Bierbrauen: Anmeldung der gebrauten Mengen muss bei jedem Sud erfolgen
  - Biomüll wird nicht direkt rausgebracht, sondern sammelt sich in der KücheSeitenumbruch

## 3.2. Die Erschaffung der Personas

Um aus den Nutzerinteressen einzelne Stakeholder abzuleiten, wird die Personas-Methode angewendet.  
Dazu wurden die Anwendungsfälle in einer Matrix mit den möglichen Personas angeordnet, um so zu erkennen welche Anwendungsfälle sich gut ergänzen (siehe Tab. 1).

Tab. 1: Anwendungsfälle-Personas Matrix

| Idee / Persona                                     |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |  10   |  11   |  12   |  13   |  14   |
| :------------------------------------------------- | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| Dunkelkammer für Textilwerkstatt                   |   x   |       |       |       |       |       |       |       |       |       |       |       |       |       |
| molekulare Küche (Bubble Tee/Melonen Kaviar)       |       |       |   x   |       |       |       |       |       |       |       |       |       |       |       |
| Lagerung von sauberen Maschinen                    |       |       |       |       |       |       |       |       |       |   x   |       |       |       |       |
| verdeckte Lagerung von Kamera Material             |       |       |       |       |       |       |       |       |       |   x   |       |       |       |       |
| Bierbrauen                                         |       |   x   |       |       |       |       |       |       |       |       |       |       |       |       |
| vegane Lebensmittel herstellen                     |       |       |       |   x   |       |       |       |       |       |       |       |       |       |       |
| Vakuuminfusion                                     |       |       |   x   |       |       |       |       |       |       |       |       |       |       |       |
| Gefriertrocknen                                    |       |       |   x   |       |       |       |       |       |       |       |       |       |       |       |
| Foodhacking                                        |       |       |   x   |   x   |       |       |       |       |       |       |       |       |       |       |
| Mikroskopie                                        |       |   x   |   x   |       |       |       |       |       |       |       |       |       |       |       |
| Kochen                                             |       |       |       |   x   |       |       |   x   |       |       |       |       |       |       |       |
| Wecken                                             |       |       |       |       |   x   |       |       |       |       |       |       |       |       |       |
| Dörren                                             |       |       |       |       |   x   |       |       |       |       |       |       |       |       |       |
| (Brot) Backen (Backofen) (Hohe Temperaturen?)      |       |       |       |       |   x   |       |       |       |       |       |       |       |       |       |
| Backstein für Ofen? Elektro-Steinbackofen?         |       |       |       |       |   x   |       |       |       |       |       |       |       |       |       |
| Tiefziehbox                                        |       |       |       |       |       |       |       |       |       |       |       |       |   x   |       |
| Fermentieren                                       |       |   x   |       |       |   x   |       |       |       |       |       |       |       |       |       |
| Sitzgelegenheit/ruhiger Arbeitsplatz               |       |       |       |       |       |   x   |   x   |       |       |   x   |       |       |       |       |
| Kaffee etc. rösten                                 |       |       |   x   |       |       |       |       |       |       |       |       |       |       |       |
| Mixen (Drinks etc.)/Blender                        |       |       |   x   |       |   x   |       |       |       |       |       |       |       |       |       |
| Papierarbeit (Papierschöpfen etc.)                 |       |       |       |       |       |       |       |       |   x   |       |       |       |       |       |
| Tastings (z.B. Bier)                               |       |   x   |       |       |       |       |       |       |       |       |       |       |       |       |
| Dämpfen                                            |       |       |       |       |       |       |   x   |       |       |       |       |       |       |       |
| Sous-Vide-Garen                                    |       |       |   x   |       |       |       |   x   |       |       |       |       |       |       |       |
| Branntwein brennen? (Anmeldepflichtig!)            |       |   x   |       |       |       |       |       |       |       |       |       |       |       |       |
| Aromatisieren durch Einlegen (Essig, Öl, Rum, ...) |       |       |       |       |   x   |       |       |       |       |       |       |       |       |       |
| Seife und andere Pflegeprodukte herstellen         |       |       |       |       |       |       |       |   x   |       |       |       |       |       |       |
| Filme auf dem Beamer schauen/Chaos Everywhere      |       |       |       |       |       |       |       |       |       |   x   |       |       |       |       |
| Pflanzen (Pilze, Kräuter, ...) in der Küche        |       |       |       |       |       |       |       |       |       |       |   x   |       |       |       |
| Tiefkühlpizza oder so zubereiten                   |       |       |       |       |       |       |       |       |       |       |       |   x   |       |       |
| Zero Waste                                         |       |       |       |       |       |       |       |   x   |       |       |       |       |       |       |
| Tonarbeiten/Modellieren                            |       |       |       |       |       |       |       |       |       |       |       |       |       |   x   |

Zu den so identifizierten Personas wurde sich noch eine kurze Hintergrundgeschichte ausgedacht, die auf der Motivation der einzelnen Anwendungsfälle aufbaut. Die Gesichter wurden über die Fakeface API (hankhank10 28.06.2021) abgerufen und Namen mit dem Random Name Generator (Campbell/Campbell 28.06.2021) ausgewählt (siehe Tab. 2).

Tab. 2: Die Personas im Überblick

|   #   | Persona                                                                                   | Beschreibung                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| :---: | :---------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|   1   | ![](https://content.fakeface.rest/female_21_f9db535404b41c14116badadc4eecad137ab98d7.jpg) | Nadja (21) interessiert sich für Nerd-Shirts, sie liebt es mit den einfallsreichsten Sprüchen oder kompliziertesten Mustern Leute aufmerksam zu machen und zum Schmunzeln zu bringen. Nach vielen ausgewaschenen T-shirts reicht ihr das Konsumieren nicht mehr, sie möchte selbst kreativ werden. Sie hatte sich schon sehr auf das Siebdruckset aus der Textilwerkstatt gefreut und der abgedunkelte Raum ermöglicht es ihr nun endlich dies auch einzusetzen.                                                                                                                                                      |
|   2   | ![](https://content.fakeface.rest/female_37_6ff04876e3a9ab67f89f4d930b4ce980be2ea660.jpg) | Lucy (37) trinkt gerne Bier und ist fasziniert von der Komplexität, die es entwickeln kann. Angeblich ist es vielfältiger als Wein. Sie hat auch schon einige Craft Biere getestet und möchte nun selbst brauen. Das ist eigentlich nicht schwer, aber als Lebensmitteltechnikerin möchte sie auch die Hefezellen untersuchen und kombinieren, um noch mehr Vielfalt zu erzeugen. Als langfristiges Projekt möchte sie auch Whisky selbst herstellen. der unterscheidet sich am Anfang gar nicht so sehr von Bier, braucht nur viel länger, bis er fertig ist.                                                        |
|   3   | ![](https://content.fakeface.rest/male_43_99e724db00a715ce67c07d7bfe280c7a101d72d6.jpg)   | Oliver (43) ist Koch in einer großen Kantine, merkt aber, dass er sich in seiner Leidenschaft eingeschränkt fühlt. Im Makerspace möchte er wieder kreativ mit Lebensmitteln werden, bald seine eigenen convenience Produkte herstellen und ausprobieren was die Molekularküche so zu bieten hat.                                                                                                                                                                                                                                                                                                                      |
|   4   | ![](https://content.fakeface.rest/male_43_f46f3804853eebd96d11503f7618870dffc33e6f.jpg)   | Rainer (43) ist nach dem letzten Tönnies Skandal Veganer geworden, aber unglücklich mit dem Angebot, das er findet. Er möchte sein Glück versuchen und eigenen veganen Käse herstellen und herausfinden wie das Beyond Meat produziert wird.                                                                                                                                                                                                                                                                                                                                                                          |
|   5   | ![](https://content.fakeface.rest/male_29_ed9b652b75f6f84a25b2b0b2da8e72bd32fdf35b.jpg)   | Rupert (29) ist durch seine Arbeit viel bei Kunden unterwegs  und möchte dabei nicht auf selbstgekochte Gerichte verzichten. In der Kantine ist das sogenannte Essen doch eher gewöhnungsbedürftig. Er möchte Lebensmittel auf Vorrat einkochen, um diese dann bei Bedarf aufzuwärmen, insbesondere in der Mittagspause. Dies ermöglicht es ihm am Wochenende ganz seiner Leidenschaft des Kochens nachzugehen und diese unter der Woche zu genießen. Wenn er dabei noch mit anderen richtig große Mengen zubereiten kann und auch neue Dinge über die Haltbarmachung wie Dörren oder Fermentieren lernt umso besser! |
|   6   | ![](https://content.fakeface.rest/female_24_743c6f7b02ce8cd26a0bb5a6106ad171851b16d3.jpg) | Katharina (24) Möchte sich gerne mal zurückziehen und alleine oder in einer Kleingruppe arbeiten. Im Hauptraum ist es ihr oft zu laut oder zu hektisch, um in Ruhe zu konstruieren oder zu schreiben.                                                                                                                                                                                                                                                                                                                                                                                                                 |
|   7   | ![](https://content.fakeface.rest/male_31_fb9edc6f62d75acf4e5c2e853a6a2465be8f1c4c.jpg)   | Dennis (31) war schon immer der Meinung, dass in der Küche die besten Partys stattfinden. Er kocht regelmäßig zusammen mit seinen Freunden, allerdings werden dabei nur selten neue Dinge ausprobiert, sondern sich auf die Klassiker beschränkt. Im Makerspace möchte er mit anderen die neusten Trends kochen, auch wenn diese mal nicht den Geschmack treffen.                                                                                                                                                                                                                                                     |
|   8   | ![](https://content.fakeface.rest/female_11_752b55b59dee3bc2ab023ec85ee00ada4a3c5128.jpg) | Silvia (11) engagiert sich bei Fridays for Future und ärgert sich darüber, dass so viele Produkte in Unmengen Verpackung daherkommen oder Zusatzstoffe haben, die nicht unbedingt gut für sie selbst oder die Umwelt sind. Sie möchte gerne eigene Seife und Körperpflegeprodukte herstellen. Sie ist noch sehr jung und traut es sich nicht alleine zu das anzugehen. Zudem ist es auch effizienter, wenn gleich eine größere Menge hergestellt wird. Ob die anderen sie wohl unterstützen? Schließlich ist es für eine gute Sache und gegen fettige Haare hilft es auch noch!                                       |
|   9   | ![](https://content.fakeface.rest/male_34_012eb56186f33cd4547b73141dd45e98564a0c6e.jpg)   | Peng (34) hat einmal bei Galileo gesehen wie Eierpappen hergestellt werden. Eigentlich ist das gar nicht so schwer und es ist dann eine wirklich biologisch abbaubare Verpackung. Er würde auch gerne einmal Papierschöpfen, das funktioniert ganz ähnlich und es lässt sich dann mit gepressten Blüten verzieren.                                                                                                                                                                                                                                                                                                    |
|  10   | ![](https://content.fakeface.rest/female_46_bb269c80a71e193ec62e0457f90eec0dbf8692d3.jpg) | Laura (46) dreht ihre eigenen Kurzfilme und macht Dokumentationen zu den Projekten, die im Makerspace umgesetzt werden. Leider hatte sie bisher keinen passenden Raum, um diese zu zeigen. Mit der Küche wäre dies Ideal, es ist schön Dunkel und eine passende Wand gibt es auch, es fehlen nur ein paar Sofas. Bisher schaut sie sich die Vorträge vom Chaos Communication Congress alleine zu Hause an, wenn sie nicht hinfahren kann. In anderen Hackerspaces wird das als Chaos Everywhere zelebriert und man schaut sich gemeinsam die Vorträge an und diskutiert darüber.                                      |
|  11   | ![](https://content.fakeface.rest/female_26_ff3b7a3649ab7a0aa5f9fe27f83b34e54ffbec49.jpg) | Henriette (26) möchte gerne eigene Pilze züchten oder einmal testen, wie es mit einer Hydrokultur oder Algenfarm funktioniert. In Ihrer Wohnung hat sie nicht den Platz dafür, aber in der Küche könnte es funktionieren. Der Raum ist recht hoch und der Platz über dem Eingangsbereich kann doch bestimmt eh nicht besser verwendet werden...                                                                                                                                                                                                                                                                       |
|  12   | ![](https://content.fakeface.rest/male_29_32eadadac1998a9411cb1d6b1b3fea176aea897e.jpg)   | Eugen (29) bastelt gerne an seinen Elektroprojekten im Space, oft von nachmittags bis spät nachts. Leider vergisst er dabei das Abendessen. Aber wenn es nun doch eine Küche gibt, könnte man dort auch schnell etwas zubereiten. Ganz Nerd-Like eine Tiefkühlpizza!                                                                                                                                                                                                                                                                                                                                                  |
|  13   | ![](https://content.fakeface.rest/male_46_a34707fde418a7331e762acce6db4f3f3fa791dd.jpg)   | Gerhardt (46) sammelt Korkenzieher verschiedener Epochen und Stile. In seiner Wohnung präsentiert er diese in einer Vitrine. Leider hat er viel mehr als dort hineinpassen. Er wechselt sie zwischendurch aus und muss sie sorgfältig verstauen, damit sie nicht beschädigt werden. Momentan sind sie einzeln in Papier eingewickelt und von außen nicht zu unterscheiden. Es möchte sich eigene Einlagen für seine Aufbewahrungsbox mittels Tiefziehens herstellen. Dann hat jeder Korkenzieher seinen Platz und es ist erkennbar welcher wo ist.                                                                    |
|  14   | ![](https://content.fakeface.rest/female_37_d427c5eedf3d9e9d364db85003db8125d7602eb4.jpg) | Christiane (37) hat als Kind schon gerne mit Knete gespielt und möchte nun eigene Figuren und Schalen aus Ton und FIMO herstellen. Letzteres lässt sich in einem normalen Backofen "brennen" und ist einsteigerfreundlich. Danach muss der Ofen nur gut ausgelüftet werden bevor wieder Lebensmittel hinein dürfen.                                                                                                                                                                                                                                                                                                   |

## 3.3. Entwickeln eines Plans zur Einbindung der Stakeholder

Insgesamt ergeben sich 26 Stakeholder. Diese wurden in einem Register erfasst und kollaborativ hinsichtlich Interesse, Einfluss, Einstellung und Engagement bewertet. Daraus ergab sich eine Priorisierung für die einzelnen Stakeholder. Viele der Stakeholder wissen zu der Projektinitialisierung noch nichts von dem Projekt und daher kann ihr Engagement vorerst nur mit "Uanware" eingestuft werden. Die Weberei und die Stadt Gütersloh wurden eher negativ dem Projekt gegenüber eingestuft, da das Projekt für sie Aufwand bedeutet, ohne einen konkreten Nutzen zu bringen. Das Interesse der Personas wurde durch eine Umfrage unter den Mitgliedern eingeschätzt.

Tab. 3: Stakeholder Register

|   #   | Stakeholder          | Interesse | Einfluss | Einstellung | Priorität | unaware | resistant | neutral | supportive | leading |
| :---: | :------------------- | :-------: | :------: | :---------: | :-------: | :-----: | :-------: | :-----: | :--------: | :-----: |
|   1   | Makerspace Gütersloh |   hoch    |   hoch   |   positiv   |     1     |         |           |         |            |   dc    |
|   2   | Die Weberei          |  neutral  |   hoch   |  negativ?   |     2     |    c    |           |         |     d      |         |
|   3   | Heimatscheck         |  neutral  |   hoch   |   positiv   |     2     |    c    |           |         |     d      |         |
|   4   | Projektkoordinator   |   hoch    |   hoch   |   positiv   |     1     |         |           |         |            |   dc    |
|   5   | Dr. Oetker           |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|   6   | Schenke              |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|   7   | Nobilia              |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|   8   | Mestemacher          |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|   9   | Brauhaus Gütersloh   |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|  10   | Miele                |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|  11   | Stadt Gütersloh      |    ???    |   hoch   |  negativ?   |     2     |    c    |           |         |     d      |         |
|  12   | Suppenküche e.V.     |    ???    |  gering  |   positiv   |     4     |    c    |           |    d    |            |         |
|  13   | Nadja                |    44%    | neutral  |   positiv   |     3     |    c    |           |         |            |    d    |
|  14   | Lucy                 |    33%    | neutral  |   positiv   |     3     |    c    |           |         |     d      |         |
|  15   | Oliver               |    44%    | neutral  |   positiv   |     3     |    c    |           |         |            |    d    |
|  16   | Rainer               |    22%    | neutral  |   positiv   |     3     |    c    |           |         |     d      |         |
|  17   | Rupert               |    22%    | neutral  |   positiv   |     3     |    c    |           |         |     d      |         |
|  18   | Katharina            |    66%    | neutral  |   positiv   |     3     |    c    |           |         |            |    d    |
|  19   | Dennis               |    77%    | neutral  |   positiv   |     3     |    c    |           |         |            |    d    |
|  20   | Silvia               |    55%    | neutral  |   positiv   |     3     |    c    |           |    d    |            |         |
|  21   | Peng                 |    33%    | neutral  |   positiv   |     3     |    c    |           |    d    |            |         |
|  22   | Laura                |    44%    | neutral  |   positiv   |     3     |    c    |           |    d    |            |         |
|  23   | Henriette            |    11%    | neutral  |   positiv   |     3     |    c    |     d     |         |            |         |
|  24   | Eugen                |    33%    | neutral  |   positiv   |     3     |    c    |     d     |         |            |         |
|  25   | Gerhard              |    44%    | neutral  |   positiv   |     3     |    c    |           |    d    |            |         |
|  26   | Christiane           |    33%    | neutral  |   positiv   |     3     |    c    |           |    d    |            |         |


Um die Stakeholder besser kennen zu lernen wurde ein Maßnahmenplan erstellt:

- zu 2) Der Weberei wird mitgeteilt, dass der Raum angemietet wird und ein Umbau bzw. eine Instandsetzung erforderlich ist. Dabei muss ein Wegerecht vereinbart werden, da der Raum den einzigen Zugang zum Netzwerkraum darstellt. Damit sich die Weberei nicht gegen das Projekt stellt soll die Koordination vom Umbau und Instandsetzung durch den Makerspace erfolgen, damit möglichst wenig Aufwand für die Weberei entsteht. Zudem könnte angeboten werden, dass die Gastronomie sich auch in der Experimentalküche ausprobieren darf. Es muss auch betont werden, wie wichtig es für das Projekt ist, dass die Weberei sich nicht in den Weg stellt, sondern positiv das Projekt unterstützt oder nur passiv gewähren lässt.
- zu 3) Für den Heimatscheck muss ein Förderantrag gestellt werden, der darlegt was für Maßnahmen mit dem Projekt geplant sind und wie sich ein Heimatbezug dazu herstellen lässt.
- zu 5-10) bei den Potenziellen Förderern müssen Kontaktpersonen ausgemacht werden, um möglichst direkt zu kommunizieren. Dafür sollen Kontakte aus den Vereinsmitgliedern genutzt werden, sofern vorhanden.
- zu 11) Die Stadt muss den Umbauten bzgl. Abluft zustimmen, da sonst keine Küchenarbeiten in einem innenliegenden Raum möglich sind. In der Vergangenheit wurden bereits Gespräche zwischen dem Verein und der Ehrenamtsbeauftragten der Stadt sowie der Vorsitzenden des Ausschusses für Kultur und Weiterbildung geführt, bei denen sich herausgestellt hat, dass der Makerspace als Bereicherung für die Stadt gesehen wird. Dies muss bei der Kommunikation auch so vorgetragen werden Stichwort "gemeinnützige Werkstatt & Küche für alle". Es kann auch helfen, wenn wir erzählen, dass die Suppenküche davon profitiert.
- zu 12) Die Suppenküche soll informiert werden, dass ein neuer Raum im Space erschlossen wird. Dies kann für die dort arbeiten Menschen interessant sein oder für das Vorhaben an sich, da es auch Möglichkeiten zum Haltbar machen von Lebensmitteln gibt, die dann wiederrum ausgegeben werden können.
- zu 13-26) Das Interesse an den einzelnen Personas wird unter den Mitgliedern mit einer weiteren Umfrage erfasst, da bisher nur neun Personen dazu abgestimmt haben. Bei einer angemessenen Beteiligung können dann die Ziele abgeleitet werden. Da Platz und Budget stark begrenzt sind werden nicht alle Anwendungsfälle umgesetzt werden können. Mit den Personen, die sich für ein nicht priorisiertes Thema interessieren, muss dann im Besonderen kommuniziert werden, damit sich diese nicht gegen das Projekt stellen, sondern sich trotzdem dafür einsetzen mit dem Wissen, dass es sich um eine Grundausstattung handelt und bei Gelegenheiten neue Ausstattung eingerichtet werden kann. So wie es in den anderen Werkstattbereichen auch der Fall ist.

# 4. Fazit

Das Stakeholder Management nach PMBOK ist ein hilfreiches Instrument, um die Projektbeteiligten zu erkennen, zu analysieren und einzubeziehen. Da es sich um regelmäßige Ereignisse handelt besteht kein Anspruch auf Vollständigkeit der Informationen. Stattdessen soll situativ auf Änderungen im Projekt reagiert werden. Es bietet Prozesse für ein strukturiertes Vorgehen, um die Informationen zu visualisieren, dokumentieren und einen Plan zu entwickeln. Es wird ersichtlich welche Stakeholder einen hohen Einfluss auf das Projekt haben und deswegen bevorzugt behandelt werden sollten. Für das Projektteam schafft das Stakeholder Management Transparenz und trägt dadurch zu einem Verständnis für die Projektziele bei.

In Rahmen dieses Projektes gilt die Besonderheit, dass die Nutzer der Projektergebnisse zugleich das Projektteam sind. Die Persona-Methode ist hilfreich, um diesen Umstand zu entflechten. Es wurden fiktive Personen erschaffen und für die Zielgruppenanalyse herangezogen. Dadurch wurden Vorwürfe innerhalb des Teams unterbunden, da Argumentationen im Namen einer Persona erfolgt sind, welche gemeinschaftlich erschaffen wurde.  
Im Bezug auf die Maker-Bewegung ist die Persona-Methode besonders gut geeignet, da die verschiedenen Kreativitätstechniken spielerisch eingesetzt werden können und damit eine Grundeigenschaft von Hatch bedient wird. Die Zusammenarbeit erfordert eine Koordination der Maker, jedoch sind diese dann intrinsisch motiviert sich daran zu beteiligen. Durch die unterschiedlichen Individuen ist es wahrscheinlich, dass die identifizierten Stakeholder die Vielfalt darstellen, die erforderlich ist. Für die Erstellung der Gesichter und Namen konnten digitale Werkzeuge genutzt werden, zu denen dann eine Hintergrundgeschichte erfunden wurde. Dies bietet neben den Interessen auch einen Einblick für die Motivation der Persona. Mit dieser Motivation ist es möglich sich in die Persona hineinzuversetzen und selbst ein Interesse zu entwickeln.

Mit dem formulierten Plan zu Einbindung der Stakeholder wurden die nächsten Schritte für den Projektablauf definiert:

- Analysieren des Engagements der Personas durch eine weitere Umfrage
- Ausformulieren der Projektziele
- Ansprechen der Weberei und der Stadt hinsichtlich der Renovierungen/Umbauten für das Projekt
- Ermitteln der Kontaktpersonen bei sonstigen Stakeholdern

Zusammenfassend kann festgehalten werden, dass ein Stakeholder Management in Kombination mit der Personas Methode zur Konkretisierung von Projektzielen innerhalb der Maker-Bewegung geeignet ist und für weitere Projekte genutzt werden sollte. Es bietet ein strukturiertes und transparentes Vorgehen, um Interessen, Motivationen und Risiken zu erkennen und so den Projekterfolg sicherer zu machen.

# 5. Literatur- und Quellenverzeichnis

|                                             |                                                                                                                                                                                                                                                                                                    |
| ------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Biedermann, Hubert (2016)                   | Industrial Engineering und Management: Beiträge des Techno-Ökonomie-Forums der TU Austria, Wiesbaden 2016                                                                                                                                                                                          |
| Braune, Paul/Alberternst, Christiane (2013) | Führen im öffentlichen Bereich und in Non-Profit-Organisationen: Handeln zwischen Politik und Verwaltung – Instrumente und Arbeitsfelder, Wiesbaden 2013                                                                                                                                           |
| Brüning, Stephan/Schwanitz, Johannes (2018) | Projektmanagement: Lerneinheit 1: Einführung in das Projektmanagement, Südwestfalen 2018                                                                                                                                                                                                           |
| Campbell, Mike/Campbell, Tara (28.06.2021)  | Random Name Generator, https://www.behindthename.com/random/ (Zugriff: 28.06.2021)                                                                                                                                                                                                                 |
| Cooper, Alan (2004)                         | The Inmates Are Running the Asylum: Why High-Tech Products Drive Us Crazy and How to Restore the Sanity, Indiana 2004                                                                                                                                                                              |
| DIN (2009)                                  | DIN 69901-5:2009-01, Projektmanagement: Projektmanagementsysteme: Teil 5: Begriffe, Hrsg. DIN e.V., Berlin, 2009                                                                                                                                                                                   |
| Fab Foundation (28.06.2021)                 | More Than 1,750 Fab Labs all over the Globe!, https://fabfoundation.org/global-community/#fablab-map (Zugriff: 28.06.2021)                                                                                                                                                                         |
| Fastermann, Petra (2014)                    | 3D-Drucken: Wie die generative Fertigungstechnik funktioniert, Berlin Heidelberg 2014                                                                                                                                                                                                              |
| hackerspaces.org (28.06.2021)               | hackerspaces.org, https://hackerspaces.org (Zugriff: 28.06.2021)                                                                                                                                                                                                                                   |
| hackerspaces.org (28.06.2021a)              | Hackerspaces in Germany, https://wiki.hackerspaces.org/Germany (Zugriff: 28.06.2021)                                                                                                                                                                                                               |
| hankhank10 (28.06.2021)                     | fakeface: An RESTful API for thispersondoesnotexist, https://github.com/hankhank10/fakeface (Zugriff: 28.06.2021)                                                                                                                                                                                  |
| Hatch, Mark (2013)                          | The Maker Movement Manifesto, New York 2013                                                                                                                                                                                                                                                        |
| Lepzien, Josefine/Lewerenz, Michael (2017)  | Persona-Methode: Eine Methode zu Illustrierung von Bildungsbedarfen, in: Weiterbildungmanagement professionalisieren, Rostock 2017                                                                                                                                                                 |
| Maker Media (28.06.2021)                    | Was sind Maker?, https://maker-faire.de/was-sind-maker/ (Zugriff: 28.06.2021)                                                                                                                                                                                                                      |
| Maker Media (28.06.2021a)                   | Makerspaces, Fablabs und andere Orte der Maker-Community. https://maker-faire.de/makerspaces/ (Zugriff: 28.06.2021)                                                                                                                                                                                |
| Makerspace Gütersloh (27.02.2021)           | Protokoll der ordentlichen Mitgliederversammlung des Makerspace Gütersloh e.V., https://github.com/makerspace-gt/protokolle/blob/main/2021/2021-02-27%20Mitgliederversammlung.md#top7---vereinsziele-f%C3%BCr-das-jahr-2021, (Zugriff: 29.06.2021)                                                 |
| Makerspace Gütersloh (29.08.2021)           | Über uns, https://makerspace-gt.de/ueber-uns/ (Zugriff: 29.06.2021)                                                                                                                                                                                                                                |
| MHKBG (2021)                                | Heimat. Zukunft. Nordrhein-Westfalen. Wir fördern, was Menschen verbindet., Hrsg. Ministerium für Heimat, Kommunales, Bauund Gleichstellung des Landes Nordrhein-Westfalen, https://www.mhkbg.nrw/sites/default/files/media/document/file/FAQ_Heimatfoerderung04.03.2021.pdf (Zugriff: 06.02.2021) |
| MIT Center for Bits and Atoms (2012)        | The Fab Charter, http://fab.cba.mit.edu/about/charter/ (Zugriff: 28.06.2021)                                                                                                                                                                                                                       |
| PA/GPM (2004)                               | Erfolgreich Projekte durchführen, Hrsg. PA Consulting Group und GPM Deutsche Gesellschaft für Projektmanagement e. V., in projektmagazin, www.projektmagazin.de 2004                                                                                                                               |
| PMI (2017)                                  | A Guide to the Project Management Body of Knowledge: PMBOK guide, Hrsg. Project Management Institute, Inc., 6. Ausgabe, Newtown Square 2017                                                                                                                                                        |